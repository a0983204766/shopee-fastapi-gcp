import os
import pymysql.cursors

from typing import Union
from dotenv import load_dotenv
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

load_dotenv()

class Item(BaseModel):
    username: str
    age: int

connection = pymysql.connect(unix_socket=os.getenv("MYSQLUNIXSOCKET"),
                             user=os.getenv("MYSQLUSER"),
                             password=os.getenv("MYSQLPASSWORD"),
                             database=os.getenv("MYSQLDATABASE"),
                             cursorclass=pymysql.cursors.DictCursor)


with connection.cursor() as cursor:
    sql = """
    CREATE TABLE IF NOT EXISTS users (
        id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        username VARCHAR(16) NOT NULL,
        age int(3) NOT NULL,
        PRIMARY KEY (id)
    )
    """
    cursor.execute(sql)
    connection.commit()

app = FastAPI()


@app.post("/users")
def insert_new_user(item: Item):
    with connection.cursor() as cursor:
        try:
            sql = "INSERT INTO `users` (`username`, `age`) VALUES (%s, %s)"
            cursor.execute(sql, (item.username, item.age))
            connection.commit()
            return "insert user successfully2"
        except Exception as e:
            connection.rollback()
            raise HTTPException(status_code=500, detail="Failed to insert user")


@app.delete("/users/{id}")
def delete_user(id: int):
    with connection.cursor() as cursor:
        try:
            sql = "DELETE FROM `users` WHERE id = %s"
            cursor.execute(sql, (id, ))
            connection.commit()
            return "delete user successfully"
    
        except Exception as e:
            connection.rollback()
            raise HTTPException(status_code=500, detail="Failed to delete user")

@app.put("/users/{id}")
async def update_user(id: int, item: Item):
    with connection.cursor() as cursor:
        try:
            sql = "UPDATE `users` SET username = %s, age = %s WHERE id = %s"
            result = cursor.execute(sql, (item.username, item.age, id))
            connection.commit()
            return "update user successfully"
    
        except Exception as e:
            connection.rollback()
            raise HTTPException(status_code=500, detail="Failed to update user")



@app.get("/users")
def get_all_users():
    with connection.cursor() as cursor:
        try:
            sql = "SELECT `id`, `username`, `age` FROM `users`"
            cursor.execute(sql)
            result = cursor.fetchall()
            return result

        except Exception as e:
            connection.rollback()
            raise HTTPException(status_code=500, detail="Failed to get users")

@app.get("/users/{id}")
def get_certain_user(id: int):
    with connection.cursor() as cursor:
        try:
            sql = "SELECT `id`, `username`, `age` FROM `users` WHERE id = %s"
            cursor.execute(sql, (id, ))
            result = cursor.fetchone()
            return result

        except Exception as e:
            connection.rollback()
            raise HTTPException(status_code=500, detail="Failed to get certain user")
