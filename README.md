cloud run: https://app-sxp23x4jaq-de.a.run.app

load-balancer 前端靜態ip:  http://34.111.81.13/docs

​    

### 流程概念

1. 透過gitlab ci功能，把我們要部屬的東西先丟到google提供的鏡像裡面 (能夠操作 gcloud)，並且設置相關權限與設定
2. 然後建構出鏡像，內容預設從Dockerfile的檔案開始製作
3. 之後push 到artifact registry上面
4. 之後從 artifact registry 抓取該鏡像後deploy 到cloud run

​    

## 部屬應用

新開一個專案

設定cloud build

```
1. 啟動 cloud build api
2. 開啟部分 cloud build 服務帳戶權限: 
   - Cloud Run 管理員 (+ 啟動Cloud Run Admin API)
   - 服務帳戶使用者
```

> 允許cloud build deploy 到cloud run + 使用服務帳戶權限

​    

回頭設定服務帳戶權限

```
1. 建立服務帳戶
2. 設置存取權
	- Cloud Build 服務代理  => 授予代管資源存取權
	- Cloud Build 編輯者 => 可建立及取消版本
3. 下載其金鑰 (取得該服務帳戶的存取權限)
```

> 針對cicid這件事情，使用該服務帳戶進行相關權限授權，其他事件或許就開另一個乾淨的服務帳戶進行相關授權

​    

設置cloud mysql

```
1. 啟用 sql 的 api
2. 其他依據預設方案後建立即可 
	(連線: 預設為公開 ip，沒有要設定私有雲ip)
	
3. 之後透過cloud shell，先開進去創建database
	(使用cloud shell 會跳出需要啟動Cloud SQL Admin API，啟動他才能在cloud shell跟mysql資料庫連線)
```

> 這裡單純要先建立database的考量是因為之後的開發，應該比較少會需要持續更換database (table可能需要，所以table的創建我寫在程式裡面)

​    

設定gitlab的環境變數

```
1.設置yaml檔案所需的環境變數
	- 服務帳戶的金鑰內容
	- 專案的ID
	- mysql的連線名稱
	- mysql在pymysql上使用的unixsocket名稱 (/cloudsql/連線名稱)
	- mysql密碼
	- mysql使用者
	- mysql資料庫
	(可參考 .gitlab-ci.yml的檔案)
```

> 在gcloud builds submit 時 搭配 --substitutions丟入存在gitlab上的變數到cloudbuild.yaml

​    

gitlabCI

```
1. clone專案
2. push他
```

> 都於yml檔案設定好了，所以無需作其他手動作業

​    

建立cloud run的 load-balancer

```
1. 虛擬私有雲網路: 設置靜態ip
2. 建立負載平衡器
	(https)
	(從網際網路傳送至 VM 或無伺服器服務)
	(設置前端: ip地址選擇剛剛建立的靜態ip，其他預設)
	(設置後端: 後端類型 => "無伺服器網路端點群組")
	(新增後端: 建立無伺服器網路端點群組 => "cloud run" 選取服務選擇建立的cloud run服務名稱)
3. 其他預設後 建立即可，之後透過前端的靜態ip就可以達到 load-balance的功能
```

​    

---

### 其他檔案細節補充說明

main.py

```
連線方式用 pymysql本身就有接收unix_socket的方式，就不用使用那長長的範例連線方式了
```

​    

cloudbuild.yaml

```
額外增設logging: CLOUD_LOGGING_ONLY 是為了讓log僅寫入到 google cloud 的日誌服務，不會在本地系統留下任何log
```

​    

---

### 針對題目要求

1. **使用 Python FastAPI 框架建立具有 CRUD 功能的簡易 REST API**

   ```
   請參考 main.py的檔案
   url: http://34.111.81.13/docs
   ```

   ​    

2. **資料庫連接**

   ```
   先手動建立好cloud mysql (包含database)
   之後透過 cloudbuild.yaml 去deploy cloud run時連接
   ```

   > '--add-cloudsql-instances=$_MYSQLNAME',

   ​    

3. **透過 CI 工具自動部署到 Google Cloud Run**

   ```
   請參考 .gitlab-ci.yml 與 cloudbuild.yaml (記得先於gitlab上設置所需的環境變數)
   ```

   > 我看官網上的範例，針對gitlab的ci 與cd，得是 "手動" 設置 cloud build + 連接至gitlab + cloud trigger + artifact registry  與部屬cloud run
   > 就算已經做好這些流程，最後部屬到cloud run的我看沒有gitlab的選項，可能得每次commit就得上去部屬，所以改用腳本的方式一氣呵成

   ​    

4. **使用 Cloud Load Balancing 服務導入流量**

   ```
   請參考上述設置的 load balancer的步驟
   frontend : http://34.111.81.13/docs
   backend: cloud run
   ```

   ​    

5. **GCP IAM 權限管理**

   ```
   檢視者的身分
   ```

   ​    

6. **加分題目**: **使用 GitLab CI 實現部署**

   ```
   目前就是用 gitlab ci工具部屬了
   ```

   ​    

7. ##### 加分題目: **設定防止外部直接訪問 Cloud Run 頁面的規則**

   ```
   cloud run:輸入控管"得是從內部才行
   目前無法透過 cloud run 提供的url訪問，但可從負載平衡器連到cloud run
   ```

   > '--ingress=internal-and-cloud-load-balancing'

   註: 原本想說這題是不是要問防火牆的事情，但我發現cloud run 並不在vpc底下，所以就用這個方式"似乎"解決外部直接訪問，若有理解錯誤煩請告知

   ​    

8. 其他: 雲端服務的使用者權限管理

   ```
   服務: 基本上只針對cloud build做最小權限授權
   使用者: 目前設置檢視者，測試過應該都可以看到相關權限的設定，但不能進行更改
   ```

   