FROM python:3.8.10-slim

WORKDIR /app

COPY . /app

RUN pip3 install -r requirements.txt

CMD ["sh", "-c", "uvicorn main:app --reload --port=8000 --host=0.0.0.0"]